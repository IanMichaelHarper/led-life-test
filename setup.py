from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

version = '1.0'

install_requires = [
    # List your project dependencies here.
    # For more details, see:
    # 
]


setup(name='ledlifetest',
    version=version,
    description="This is a description of the package",
    long_description=README + '\n\n',
    classifiers=[
      # Get strings from
    ],
    keywords='led',
    author='Steven Christe',
    author_email='steven.christe@nasa.gov',
    url='',
    license='MIT',
    #packages=find_packages('ledlifetest'),
    packages=['ledlifetest'],
    #package_dir = {'': 'ledlifetest'}, include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)
