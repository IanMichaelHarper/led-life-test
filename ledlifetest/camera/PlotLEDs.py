# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:15:38 2017

@author: Ian
"""
import pandas as pd
import os.path
import time
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import matplotlib.dates as mdates

from ledlifetest import data_dir, num_snaps, num_leds

# create dataframe of csv data
df = pd.read_csv(os.path.join(data_dir, "snap_check_loop.csv"))

#df.plot.scatter(x='Exposure Time (usec)', y='LED1')
#df.valuecount()

# range of different coloured markers
clist = ['ro', 'bo', 'go', 'co', 'mo', 'yo', 'ko','rx', 'bx', 'gx', 'cx', 'mx', 'yx', 'kx','rs', 'bs', 'gs', 'cs']
#ideal_exp = [840, 800, 720, 1400, 80, 800, 40, 80, 760, 80, 680, 120, 240, 80 , 1200, 1400, 240, 7000]
def plot_leds_vs_date(filename=None): 
    """
    Plot LED flux against image creation date for last months worth of data
    """
    dates = []
    iList = []
    for i, date in enumerate(df['Date']):
        # get list of indices that mark last image in batch
        if df['Exposure Time (usec)'][i] == 10000:
            iList.append(i)
        # each batch has 31 images, so get creation date of last image taken in batch to represent the creation date of that batch
        if i%31 == 0:
            date_diff = date2num(datetime.strptime(df['Date'][len(df['Date'])-1], "%a %b %d %H:%M:%S %Y")) - date2num(datetime.strptime(date, "%a %b %d %H:%M:%S %Y"))
            if date_diff < 5*60: # only plot for last months worth of data (change this from 5mins to a month)
                dates.append(date2num(datetime.strptime(date, "%a %b %d %H:%M:%S %Y")))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%a %b %d %H:%M:%S %Y"))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    
    avg_flux = np.zeros((num_leds, len(iList)))
    for j in range(num_leds):
        temp = 0
        for i, index in enumerate(iList):
            # get average flux for batch of photos
            avg_flux[j][i] = np.mean(df['LED{0}'.format(j+1)][temp:index+1]/df['Exposure Time (usec)'][temp:index+1])            
            temp = index+1
        #flux = df['LED{0}'.format(j+1)][df['LED{0}'.format(j+1)] != 255]/df['Exposure Time (usec)'][-len(dates):]
        #plt.scatter(dates, df['LED{0}'.format(j+1)]/df['Exposure Time (usec)'])
        plt.plot(dates, avg_flux[j], markersize=1) # insert clist[j] here as 3rd argument for datapoints
    plt.gcf().autofmt_xdate()
    plt.ylabel('LED flux')
    plt.title('LED Flux vs. Image Creation Date')
    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)
            
def plot_leds_vs_exptime(filename=None):  
    """
    Plot LED brightness against exposure time
    """
    for j in range(num_leds):
        plt.plot(df['Exposure Time (usec)'][:num_snaps-2], df['LED{0}'.format(j+1)][:num_snaps-2]) #exclude last two datapoints (7000 and 10,000 usec images) since they skew the graph 
        plt.xlabel('Exposure Time (usec)')
        plt.ylabel('LED brightness')
    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)
    
while True:  #erase this loop and use loop below. This loop is just for testing purposes
    plot_leds_vs_date()
    time.sleep(30)
"""
PLOT_SLEEP_TIME = 24*3600
while True:  #should run every day
    #plot_leds_vs_exptime()
    current_time = datetime.now()
    current_month = current_time.strftime('%a %b %d %H:%M:%S %Y')
    plot_leds_vs_date(current_month +'.png')
    log.info('graph plotted, sleeping for {0} seconds'.format(PLOT_SLEEP_TIME))
    time.sleep(PLOT_SLEEP_TIME)
"""