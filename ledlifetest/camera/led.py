# -*- coding: utf-8 -*-
"""
Created on Thu Jun 08 17:29:49 2017

@author: Ian Harper
"""

from astropy.io import fits as pyfits
import matplotlib.pyplot as plt
import numpy as np
import os.path
import os
from datetime import datetime
from skimage.filters import threshold_otsu, gaussian    
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
import matplotlib.patches as mpatches
    
class LedImage(object):

    def __init__(self, filename, regions=None, box=None):
        """Initialises an LedImage object
        
        Parameters:
        -----------
        filename: str
            pathname of .fits image file
        regions: 2d image array 
            LED regions labeled as numbers 1-18
        box: 2d array 
            bounding box corners around each LED
            a row for each LED
            each row is [minx, miny, maxx, maxy] 
        """ 
        
        if regions is not None:
            self.label_image = regions
        if box is not None:
            self.bounding_box = box
            
        self.filename = filename
        fits = pyfits.open(self.filename)
        self.data = fits[1].data.astype(np.uint8)   #led data from 0 to 255
        self.meta =fits[0].header
        self.date = datetime.strptime(self.meta.get("DATE_OBS"), "%a %b %d %H:%M:%S %Y")
        self.exposure_time = self.meta['EXPOSURE']  #exposure time in microseconds

        self.led_data = []
        self.pixel_area = []

    def find_LEDs(self):
        """
        use skimage to find LED regions based on otsu method and label them 1-18
        """
        #apply gaussian filters
        #self.gauss = gaussian(self.data, sigma=1)
        
        # apply threshold
        self.thresh = threshold_otsu(self.data)
        self.bw = closing(self.data > self.thresh, square(3))

        # remove artifacts connected to image border
        self.cleared = clear_border(self.bw)

        # label image regions
        self.label_image = label(self.cleared)

        self.bounding_box = []
        self.box_centers = []
        self.labels = []
        
        self.num_leds_found = len(regionprops(self.label_image))
        for j, region in enumerate(regionprops(self.label_image)):
            # assign corners of bounding box
            minx, miny, maxx, maxy = region.bbox
            
            # find center of each bounding box
            xavg = (minx+maxx)/2
            yavg = (miny+maxy)/2
            box_center = [xavg, yavg]   
            #box_center = np.array([xavg,yavg])
            self.box_centers.append(box_center)
            
            # append data for each led to list attributes
            this_box = region.bbox
            #this_led_data = self.data[minx:maxx,miny:maxy]
            this_led_data = self.data[self.label_image == (j+1)]
            self.led_data.append(this_led_data)
            self.pixel_area.append(region.area)
            self.bounding_box.append(this_box)
            self.labels.append(region.label)
            
        self.box_centers = np.array(self.box_centers)
        return self.bounding_box

    def get_led_max(self):
        """find maximum pixel brightness for each led"""
        num_leds_found = np.array(self.bounding_box).shape[0]
        result = np.zeros(num_leds_found)
        for i, this_box in enumerate(self.bounding_box):
            this_led_data = self.get_led_data(i)
            result[i] = this_led_data.max()
        return result

    def get_led_mean(self):
        """find mean pixel brightness for each led"""
        num_leds_found = np.array(self.bounding_box).shape[0]
        result = np.zeros(num_leds_found)
        for i, this_box in enumerate(self.bounding_box):
            this_led_data = self.get_led_data(i)
            result[i] = this_led_data.mean()
        return result

    def get_led_data(self, i):
        """return led brightness as 2d array as a function of led number"""
        minr, minc, maxr, maxc = self.bounding_box[i]
        return self.data[int(minr):int(maxr), int(minc):int(maxc)]
    
    def get_BKG(self):
        """find average background brightness of image"""
        num_leds_found = np.array(self.bounding_box).shape[0]
        self.BKG = np.zeros(num_leds_found)
        for i, this_box in enumerate(self.bounding_box):
            self.BKG_data = self.data
            minr, minc, maxr, maxc = self.bounding_box[i]
            self.BKG_data[int(minr):int(maxr), int(minc):int(maxc)] -= self.get_led_data(i)
            self.BKG_max = self.BKG_data.max()
            self.BKG_mean = self.BKG_data.mean()
        return self.BKG_mean

    def plot_image(self, filename=None):
        """plot image of LEDs with bounding boxes surrounding each LED"""
        fig, ax = plt.subplots()        
        ax.imshow(self.data)
        ax.set_title("{0}, {1}usec".format(os.path.basename(self.filename), self.exposure_time))
        #ax.set_title("{0}, {1}usec, found:{2}".format(os.path.basename(self.filename), self.exposure_time, self.num_leds_found))
        #if self.num_leds_found > 0:
        for j, bbox in enumerate(self.bounding_box):
            """overlay bounding boxes on image"""
            minr, minc, maxr, maxc = bbox
            rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr, fill=False, edgecolor='red', linewidth=2)
            ax.add_patch(rect)
            ax.text(maxc+10, maxr+10, str(self.labels[j]))  #number LEDs 1-18
        if filename == None:
            plt.show()
        else:
            plt.savefig(filename)

    def plot_hist(self, filename=None):
        """plot histogram of LED brightness"""
        fig, ax = plt.subplots()
        bins = np.arange(0, 256, 5)
        plt.hist(self.data > self.get_BKG(), bins=bins)
        if self.num_leds_found > 0:
            for this_data in self.led_data:
                plt.hist(this_data[this_data > self.thresh-90].flatten(), bins=bins)
        ax.set_title("{0}, Threshold={1}".format(self.filename, self.thresh-90))
        if filename == None:
            plt.show()
        else:
            plt.savefig(filename)
