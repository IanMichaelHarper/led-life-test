# -*- coding: utf-8 -*-
"""
Created on Thu Jun 08 10:10:49 2017

@author: Ian Harper
"""
import os.path
import os
from datetime import datetime
import time
import numpy as np
from shutil import copyfile

from ledlifetest import num_snaps, log, load_box_data, find_latest_snaps, snap_led_boxes_file, num_leds, data_dir
from led import LedImage

# write headers of csv
g = open(os.path.join(data_dir, 'snap_check_loop.csv'), 'w')
g.write('Filename,Date,Exposure Time (usec),Background,')
for i in range(num_leds):
    g.write('LED')
    g.write(str(i+1))
    g.write(',')
g.write('\n')
g.close()

# initialise array to store previous led brightnesses
led_snap_brightness = np.zeros((num_snaps, num_leds))

log.info("loop started")
sleep_time = 30
while True:   #runs every day
    boxes = load_box_data(snap_led_boxes_file)   # assign bounding boxes to be used in LedImage()
    files = find_latest_snaps(1)  # get the last batch of photos taken
    if files is not None:
        log.info('Found new files {0}\n'.format(list(os.path.basename(f) for f in files)))    #make more readable
        if os.path.basename(files[0]).split('-')[1] == '40usec.fits':  # ensure batch is complete and snap.py has finished taking photos
            for i, f in enumerate(files):
                image = LedImage(f, box=boxes)
                maxb = image.get_led_max()
                
                # write image data to csv
                g = open(os.path.join(data_dir, 'snap_check_loop.csv'), 'a')
                g.write('\n')
                g.write(str(os.path.basename(image.filename))+',' + str(time.ctime(os.path.getmtime(image.filename)))+',' + str(image.exposure_time)+','+str(image.get_BKG())+',')
                for j, led in enumerate(maxb):
                    flux = led/image.exposure_time  # can be used instead of brightnesses for a different interpretation
                    g.write(str(led)+',')
                    
                    # check if any LEDs have changed brightness
                    try:
                        np.testing.assert_allclose(led, led_snap_brightness[i][j], rtol=0.05, atol=10)
                    except AssertionError:
                        log.info("LED {0} has changed brightness from {1} to {2} in file {3}".format(j+1, led_snap_brightness[i][j], led, os.path.basename(image.filename)))
                        led_snap_brightness[i][j] = led # store new brightnesses to check for next time
                #g.flush()
                g.close()
                
                # make a copy of the csv for record
                copyfile("snap_check_loop.csv", "snap_check_loop_from_{0}.csv".format(datetime.now().strftime("%Y-%m")))
        else:
            log.info('find_latest_snaps found files from last batch')
    if files is None:
        log.info('snap.py still running')
    log.info("end of loop, sleeping for {0} seconds".format(sleep_time))
    time.sleep(sleep_time)
