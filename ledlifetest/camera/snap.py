""" takes a batch of images with different exposure times and puts images in monthly folder"""

import os
import shutil
import time
import logging
import sys
import glob
from datetime import datetime

from ledlifetest import snap_exposures, log

root = logging.getLogger()
root.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(formatter)
root.addHandler(ch)

# create file handler which logs even debug messages
fh = logging.FileHandler('log_snap.log')
fh.setFormatter(formatter)
root.addHandler(fh)

UPDATE_TIME_S = 30     # snap.py will sleep for this long before taking photos again
CAMERA_SNAP_DELAY_S = 0.25   # camera delay between snapshots

SNAP_EXE = '/home/x-ray-lab/Desktop/SAAS-master/snap2'   # pathname of C++ program that takes photos
SNAP_IP = '192.168.4.4'    # IP address of camera
ANALOG_GAIN = 200     # gain setting on camera
PREAMP_GAIN = 'plus3dB'   #needs to be a number

snap_count = 0

os.chdir('/home/x-ray-lab/Documents')

while True:   #runs every day
    #os.system(SNAP_EXE)
    current_time = datetime.now()
    month_folder = current_time.strftime("%Y-%m")
    
    # make temporary folder to send the snaps to be renamed
    folder_name = 'temp' 
    
    # make monthly folder for snaps
    if os.path.isdir('/home/x-ray-lab/Documents/{0}'.format(month_folder)) == False:
        os.mkdir('/home/x-ray-lab/Documents/{0}'.format(month_folder))
    
    # make temporary folder for snaps and change into it
    if os.path.isdir('/home/x-ray-lab/Documents/{0}'.format(folder_name)) == True:
        os.chdir('/home/x-ray-lab/Documents/{0}'.format(folder_name))
    else:
        os.mkdir('/home/x-ray-lab/Documents/{0}'.format(folder_name))
        os.chdir('/home/x-ray-lab/Documents/{0}'.format(folder_name))
        
    # take photos
    for exp in snap_exposures:
        list_of_files = []
        while len(list_of_files) == 0:
            """
            if for some reason a photo is not taken, this loop will attempt to take the photo again and again until it is taken
            """
            snap_command_string = SNAP_EXE + ' ' + SNAP_IP + ' ' + str(exp) + ' ' + str(ANALOG_GAIN) + ' ' + PREAMP_GAIN
            os.system(snap_command_string)
            root.info(snap_command_string)
            root.info('Snap {0} exposure={1} usec'.format(snap_count, str(exp)))
            snap_count += 1
            
            # wait for the camera to be ready again
            time.sleep(CAMERA_SNAP_DELAY_S)
            
            # glob image and assign to list_of_files to check if image was taken
            list_of_files = glob.glob('/home/x-ray-lab/Documents/{0}/*'.format(folder_name)) 
            
        # rename file to include exposure time
        latest_file = list_of_files[0]
        renamed_file = "{0}-{1}usec.fits".format(latest_file.split('.')[0], str(exp))
        os.rename(latest_file, renamed_file)
        
        # check if monthly folder still exists in case it is deleted while photos are being taken. If it doesn't exist, create it
        if os.path.isdir('/home/x-ray-lab/Documents/{0}'.format(month_folder)) == False:
            os.mkdir('/home/x-ray-lab/Documents/{0}'.format(month_folder))
            
        # move image to monthly folder
        shutil.move(renamed_file, '/home/x-ray-lab/Documents/{0}'.format(month_folder))
        
    # change out of temporary folder
    os.chdir('/home/x-ray-lab/Documents')
    
    # print how long taking the photos took (use this to set appropriate sleep times for find_regions.py and image_check_loop.py)
    print(datetime.now() - current_time , "second have elapsed")
    
    log.info("sleeping for {0} seconds".format(UPDATE_TIME_S))
    time.sleep(UPDATE_TIME_S)
