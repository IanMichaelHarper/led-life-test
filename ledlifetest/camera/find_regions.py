import os
import numpy as np
import time

from led import LedImage
from ledlifetest import num_leds, find_latest_snaps, log, num_snaps

# initialise centers of bounding boxes to store previous locations of box centers
box_centers = np.zeros((num_leds,2))

LED_MOVE_TOLERANCE = 10   #centers of bounding boxes are allowed to move within this tolerance
REGION_CHECK_INTERVAL_S = 5*60     #time interval to check if bounding boxes have changed

while True:   #runs every month
    files = find_latest_snaps(1)   #get the last batch of photos taken
    if files is not None:
        if len(files) < num_snaps:
            log.info("first batch of photos not yet taken, checking again in 30 seconds")
            time.sleep(30)
            continue
        f = files[num_snaps-1]  #get 10,000 microsecond image
        
        #find LED regions for most saturated image
        if os.path.basename(f).split('-')[1] == "10000usec.fits":   #use basename.count? 
            image = LedImage(f)
            image.find_LEDs()
            LEDregions = image.label_image
            #label = LEDregions[LEDregions != 0]
            LEDboxes = image.bounding_box
            maxb = image.get_led_max()
            image.plot_image('bbox_latest.png')
            
            #check if regions have changed
            region_moved = False
            for j in range(image.num_leds_found):
                try:
                    np.testing.assert_allclose(image.box_centers[j], box_centers[j], atol=LED_MOVE_TOLERANCE)
                except AssertionError:
                    region_moved = True
                    log.info("LED {0} has moved from {1} to {2} in file {3}".format(j+1, box_centers[j], image.box_centers[j], os.path.basename(image.filename)))
                    box_centers[j] = image.box_centers[j]  #update location of the bounding box centers
                    
                # store regions in txt file
            if region_moved == True:
                np.savetxt("LEDboxes_temp.txt", np.array(LEDboxes), fmt='%i', delimiter=',')
                #tempfile
                os.rename('LEDboxes_temp.txt', 'LEDboxes.txt')
        else:
            log.info('find_latest_snaps() found files from last batch')
            log.info('trying again in 20 seconds')
            time.sleep(20)
            continue
        log.info("regions found, checking again in {0} seconds".format(REGION_CHECK_INTERVAL_S))
        log.info("sleeping...")
        time.sleep(REGION_CHECK_INTERVAL_S)
    else:
        log.info('files is None, checking again in 30 seconds')
        time.sleep(30)
