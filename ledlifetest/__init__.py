import os.path
import logging
import sys
from datetime import datetime
import glob
import numpy as np

#working directory path
here = os.path.abspath(os.path.dirname(__file__))
data_dir = os.path.join(os.path.dirname(here), 'ledlifetest/camera')

#set of exposure times where each LED has at least one exposure time where its brightness is around 200
snap_exposures = np.arange(40, 1000, 40)
snap_exposures = np.append(snap_exposures, np.arange(1000, 2000, 200))
snap_exposures = np.append(snap_exposures, [7000, 10000])

#file where LED bounding boxes are stored
snap_led_boxes_file = os.path.join(data_dir, 'LEDboxes.txt')

num_leds = 18
num_snaps = len(snap_exposures)

def setup_logging(filepath):
    """
    Setup up logging to a file as well as to the screen
    """
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    log.addHandler(ch)

    # create file handler which logs even debug messages
    fh = logging.FileHandler(filepath)
    fh.setFormatter(formatter)
    log.addHandler(fh)
    return log

def load_box_data(filename):
    """
    return bounding boxes from LEDboxes.txt for use in LedImage class
    """
    boxes = np.loadtxt(filename, delimiter=',')
    # safeguard to turn 1d array into 2d array if LEDboxes.txt just contains one row (Happehappens whens are off)
    if len(boxes.shape) == 1:
        boxes = np.reshape(boxes, (1,4))
    return boxes

def find_latest_snaps(TIME_DIFF_ALLOWED_S):
    """
    find the last batch of images taken by the camera
    """
    current_time = datetime.now()
    #folder_name = find_this_months_folder()
    #files = sorted(glob.iglob('/home/x-ray-lab/Documents/{0}/*'.format(folder_name)), key=os.path.getctime)
    files = sorted(glob.iglob(os.path.join(data_dir, 'snaps/snaps_for_plotting/*')), key=os.path.getctime) #need to change this once LEDs are back in chamber
    latest_files = files[-num_snaps:]
    time_diff = np.array([(current_time - datetime.fromtimestamp(os.path.getctime(f))).total_seconds() for f in latest_files])
    if np.all(time_diff > TIME_DIFF_ALLOWED_S):  # how fresh files must be compared to current time to be considered a new set of snaps
        return latest_files
    else:
        return None
        
log = setup_logging('log_image_check.log')

def find_this_months_folder():
    """
    return monthly folder
    """
    current_time = datetime.now()
    folder_name = current_time.strftime('%Y-%m')
    if os.path.isdir('/home/x-ray-lab/Documents/{0}'.format(folder_name)) == True:
        return folder_name
    else:
        log.info('{0} folder not found, creating new one'.format(folder_name))  #need to change this, will cause an error in the future
        return folder_name
    """
        f = os.mkdir(folder_name)
        return f   #should return an error since no folder means no image files?"""
