from labjack import ljm
import numpy as np
from datetime import datetime
import astropy.units as u
from time import sleep
import csv
UPDATE_TIME_S = 5
# Open first found LabJack

def create_time_string(this_time):
    return str(this_time).replace('-','').replace(' ','_').replace(':','')[:-7]

# Call eReadName to read the serial number from the LabJack.

def connect_to_labjack():
    handle = ljm.openS("ANY", "ANY", "ANY")
    name = "SERIAL_NUMBER"
    result = ljm.eReadName(handle, name)
    handle = ljm.openS("T7", "USB", "ANY")
    print("\neReadName result: ")
    print("    %s = %f" % (name, result))
    return handle

#voltages = np.zeros(80)
start_addr = 48
end_addr = 128
read_count = 0
filename = 'resistors_' + create_time_string(datetime.now()) + '.csv'
print("Creating file {0}".format(filename))

addr_str_list = ["AIN" + str(addr) for addr in np.arange(start_addr,end_addr,1)]
addr_str_list.append('AIN0')

def convert_to_pressure(analog_value):
    """Calculates the pressure in mmTorr"""
    return 10.0 ** (analog_value - 5) * 1000


with open(filename, 'w') as csvfile:
    col_names = 'time'
    for addr_str in addr_str_list:
        col_names += addr_str
    csvfile.write(col_names + '\n')
    while(True):
        sleep(UPDATE_TIME_S)
        try:
            handle = connect_to_labjack()
            csvfile.write(str(datetime.now()))
            for addr_str in addr_str_list:
                voltage = ljm.eReadName(handle, addr_str)
                #voltages[i-start_addr] = voltage
                print("{0}: {1}".format(addr_str, voltage))
                csvfile.write(',' + str(voltage))
                if addr_str == 'AIN0':
                    print("Current pressure: {0} mmTorr.".format(convert_to_pressure(voltage)))
            csvfile.write('\n')
        except ljm.LJMError:
            print("Could not connect to labjack.")
            pass
