## import the serial library
import serial
from datetime import datetime
import numpy as np
## Boolean variable that will represent
## whether or not the arduino is connected
connected = False

## establish connection to the serial port that your arduino
## is connected to.

def create_time_string(this_time):
    return str(this_time).replace('-','').replace(' ','_').replace(':','')[:-7]

locations=['/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3', '/dev/cu.usbmodem1411']

for device in locations:
    try:
        print("Trying {0}".format(device))
        ser = serial.Serial(device, 9600, timeout=1)
        break
    except:
        print("Failed to connect on {0}".format(device))

## loop until the arduino tells us it is ready
while not connected:
    serin = ser.read()
    connected = True

filename = 'temperatures_' + create_time_string(datetime.now()) + '.csv'
print("Creating file {0}".format(filename))

start_addr = 0
end_addr = 10

number_of_sensors = 18

with open(filename, 'w') as csvfile:
    col_names = 'time'
    for i in np.arange(0, number_of_sensors, 1):
        col_names += ",TEMP" + str(i)
    csvfile.write(col_names + '\n')

    while True:
        if ser.inWaiting():
            line = ser.readline().decode("utf-8")
            # check if this is a comment line
            if line[0].count('#') > 0:
                print(line)
            else:
                temperatures = line.split(',')
                print(datetime.now())
                print(temperatures)
                print(len(temperatures))
                if len(temperatures) == number_of_sensors:
                    csvfile.write(str(datetime.now()))
                    for this_temp in temperatures:
                        csvfile.write(',' + str(this_temp))
                        csvfile.flush()

## close the serial connection and text file
csvfile.close()
ser.close()
