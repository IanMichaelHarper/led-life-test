// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2
#define TEMPERATURE_PRECISION 9
#define SERIAL_BAUD_RATE 9600
#define UPDATE_RATE_S 5

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// arrays to hold device addresses
DeviceAddress tempDeviceAddress; // We'll use this variable to store a found device address
int numberOfDevices = 0;

void setup(void)
{
  // start serial port
  Serial.begin(SERIAL_BAUD_RATE);

  // Start up the library
  sensors.begin();

  while( numberOfDevices == 0 ){
    numberOfDevices = sensors.getDeviceCount();
    // locate devices on the bus
    Serial.print("# Locating devices...");
    Serial.print("# Found ");
    Serial.print(sensors.getDeviceCount(), DEC);
    Serial.println(" devices.");
    delay(UPDATE_RATE_S * 1000);
  }
  // report parasite power requirements
  Serial.print("# Parasite power is: "); 
  if (sensors.isParasitePowerMode()) Serial.println("ON");
  else Serial.println("OFF");

  for (int i = 0; i < numberOfDevices; i++){
    if (sensors.getAddress(tempDeviceAddress, i))      
      sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
      Serial.print("#");
      Serial.print(i);
      Serial.print(" ");
      Serial.print("Device Address: ");
      printAddress(tempDeviceAddress);
      Serial.println();
      //Serial.print("Device 0 Resolution: ");
      //Serial.print(sensors.getResolution(tempDeviceAddress), DEC); 
      //Serial.println();
    }
    
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

// function to print the temperature for a device
void printTemperature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  Serial.print(tempC);
}

// function to print a device's resolution
void printResolution(DeviceAddress deviceAddress)
{
  Serial.print("Resolution: ");
  Serial.print(sensors.getResolution(deviceAddress));
  Serial.println();    
}

// main function to print information about a device
void printData(DeviceAddress deviceAddress)
{
  Serial.print("Device Address: ");
  printAddress(deviceAddress);
  Serial.print(" ");
  printTemperature(deviceAddress);
  Serial.println();
}

/*
 * Main function, calls the temperatures in a loop.
 */
void loop(void)
{ 
  // call sensors.requestTemperatures() to issue a global temperature 
  // request to all devices on the bus
  // Serial.print("Requesting temperatures...");
  sensors.requestTemperatures();
  //Serial.println("DONE");

  for (int i = 0; i < numberOfDevices; i++){
    if (sensors.getAddress(tempDeviceAddress, i)){
      printTemperature(tempDeviceAddress);
      if (i < numberOfDevices-1){ Serial.print(",");}
    }
  }
  Serial.println("");
  // delay in milliseconds
  delay(UPDATE_RATE_S * 1000);
}

