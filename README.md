README
------
Code to support the LED life test.

INSTALL
-------
To install a developer edition clone this repository and run:

pip install -e .

inside the package directory.

DOCUMENTATION
-------------
Main Files:

__init__.py stores variables and functions

led.py has the LedImage() class that processes the image and finds the LEDs

snap.py takes a batch of photos every day and stores them in a monthly folder

find_regions.py finds the location of each LED in the 10,000 microsecond every month, creates a bounding box around each LED and stores bounding boxes to LEDboxes.txt. Also checks if LED location has changed

image_check_loop.py uses bounding boxes from LEDboxes.txt as LED region in LedImage(), finds the brightness of each LED within those boudning boxes and writes to csv file. Also checks if LED brightness has changed. Runs every day

PlotLeds.py Plots LED flux against image creation date every day for the last month's worth of data